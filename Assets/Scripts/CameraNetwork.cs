﻿using UnityEngine;
using UnityEngine.Networking;

public class CameraNetwork : NetworkBehaviour
{
    public Camera playercamera;
    public Transform cameraposition;

    private void Awake()
    {
        
    }
    public override void OnStartLocalPlayer()
    {
        var camera = Instantiate(playercamera, cameraposition.position, cameraposition.rotation);
        camera.transform.SetParent(this.transform);
    }

}
